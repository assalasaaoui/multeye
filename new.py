import torch
import torch.nn as nn
import time
import torch.nn.functional as F
from common import Concat, Conv, BottleneckCSP, Focus, SPP



class UpsamplingBottleneck(nn.Module):
	"""The upsampling bottlenecks upsample the feature map resolution using max
	pooling indices stored from the corresponding downsampling bottleneck.

	Main branch:
	1. 1x1 convolution with stride 1 that decreases the number of channels by
	``internal_ratio``, also called a projection;
	2. max unpool layer using the max pool indices from the corresponding
	downsampling max pool layer.

	Extension branch:
	1. 1x1 convolution with stride 1 that decreases the number of channels by
	``internal_ratio``, also called a projection;
	2. transposed convolution (by default, 3x3);
	3. 1x1 convolution which increases the number of channels to
	``out_channels``, also called an expansion;
	4. dropout as a regularizer.

	Keyword arguments:
	- in_channels (int): the number of input channels.
	- out_channels (int): the number of output channels.
	- internal_ratio (int, optional): a scale factor applied to ``in_channels``
	 used to compute the number of channels after the projection. eg. given
	 ``in_channels`` equal to 128 and ``internal_ratio`` equal to 2 the number
	 of channels after the projection is 64. Default: 4.
	- dropout_prob (float, optional): probability of an element to be zeroed.
	Default: 0 (no dropout).
	- bias (bool, optional): Adds a learnable bias to the output if ``True``.
	Default: False.
	- relu (bool, optional): When ``True`` ReLU is used as the activation
	function; otherwise, PReLU is used. Default: True.

	"""

	def __init__(self,
				 in_channels,
				 out_channels,
				 internal_ratio=4,
				 dropout_prob=0,
				 bias=False,
				 relu=True):
		super().__init__()

		# Check in the internal_scale parameter is within the expected range
		# [1, channels]
		if internal_ratio <= 1 or internal_ratio > in_channels:
			raise RuntimeError("Value out of range. Expected value in the "
							   "interval [1, {0}], got internal_scale={1}. "
							   .format(in_channels, internal_ratio))

		internal_channels = in_channels // internal_ratio

		if relu:
			activation = nn.ReLU
		else:
			activation = nn.PReLU

		# Main branch - max pooling followed by feature map (channels) padding
		self.main_conv1 = nn.Sequential(
			nn.Conv2d(in_channels, out_channels, kernel_size=1, bias=bias),
			nn.BatchNorm2d(out_channels))

		# Remember that the stride is the same as the kernel_size, just like
		# the max pooling layers
		self.main_unpool1 = nn.MaxUnpool2d(kernel_size=2)

		# Extension branch - 1x1 convolution, followed by a regular, dilated or
		# asymmetric convolution, followed by another 1x1 convolution. Number
		# of channels is doubled.

		# 1x1 projection convolution with stride 1
		self.ext_conv1 = nn.Sequential(
			nn.Conv2d(
				in_channels, internal_channels, kernel_size=1, bias=bias),
			nn.BatchNorm2d(internal_channels), activation())

		# Transposed convolution
		self.ext_tconv1 = nn.ConvTranspose2d(
			internal_channels,
			internal_channels,
			kernel_size=2,
			stride=2,
			bias=bias)
		self.ext_tconv1_bnorm = nn.BatchNorm2d(internal_channels)
		self.ext_tconv1_activation = activation()

		# 1x1 expansion convolution
		self.ext_conv2 = nn.Sequential(
			nn.Conv2d(
				internal_channels, out_channels, kernel_size=1, bias=bias),
			nn.BatchNorm2d(out_channels), activation())

		self.ext_regul = nn.Dropout2d(p=dropout_prob)

		# PReLU layer to apply after concatenating the branches
		self.out_activation = activation()

	def forward(self, x, max_indices, output_size):
		# Main branch shortcut
		main = self.main_conv1(x)
		main = self.main_unpool1(
			main, max_indices, output_size=output_size)

		# Extension branch
		ext = self.ext_conv1(x)
		ext = self.ext_tconv1(ext, output_size=output_size)
		ext = self.ext_tconv1_bnorm(ext)
		ext = self.ext_tconv1_activation(ext)
		ext = self.ext_conv2(ext)
		ext = self.ext_regul(ext)

		# Add main and extension branches
		out = main + ext

		return self.out_activation(out)




class RegularBottleneck(nn.Module):
	"""Regular bottlenecks are the main building block of ENet.
	Main branch:
	1. Shortcut connection.

	Extension branch:
	1. 1x1 convolution which decreases the number of channels by
	``internal_ratio``, also called a projection;
	2. regular, dilated or asymmetric convolution;
	3. 1x1 convolution which increases the number of channels back to
	``channels``, also called an expansion;
	4. dropout as a regularizer.

	Keyword arguments:
	- channels (int): the number of input and output channels.
	- internal_ratio (int, optional): a scale factor applied to
	``channels`` used to compute the number of
	channels after the projection. eg. given ``channels`` equal to 128 and
	internal_ratio equal to 2 the number of channels after the projection
	is 64. Default: 4.
	- kernel_size (int, optional): the kernel size of the filters used in
	the convolution layer described above in item 2 of the extension
	branch. Default: 3.
	- padding (int, optional): zero-padding added to both sides of the
	input. Default: 0.
	- dilation (int, optional): spacing between kernel elements for the
	convolution described in item 2 of the extension branch. Default: 1.
	asymmetric (bool, optional): flags if the convolution described in
	item 2 of the extension branch is asymmetric or not. Default: False.
	- dropout_prob (float, optional): probability of an element to be
	zeroed. Default: 0 (no dropout).
	- bias (bool, optional): Adds a learnable bias to the output if
	``True``. Default: False.
	- relu (bool, optional): When ``True`` ReLU is used as the activation
	function; otherwise, PReLU is used. Default: True.

	"""

	def __init__(self,
				 channels,
				 internal_ratio=4,
				 kernel_size=3,
				 padding=0,
				 dilation=1,
				 asymmetric=False,
				 dropout_prob=0,
				 bias=False,
				 relu=True):
		super().__init__()

		# Check in the internal_scale parameter is within the expected range
		# [1, channels]
		if internal_ratio <= 1 or internal_ratio > channels:
			raise RuntimeError("Value out of range. Expected value in the "
							   "interval [1, {0}], got internal_scale={1}."
							   .format(channels, internal_ratio))

		internal_channels = channels // internal_ratio

		if relu:
			activation = nn.ReLU
		else:
			activation = nn.PReLU

		# Main branch - shortcut connection

		# Extension branch - 1x1 convolution, followed by a regular, dilated or
		# asymmetric convolution, followed by another 1x1 convolution, and,
		# finally, a regularizer (spatial dropout). Number of channels is constant.

		# 1x1 projection convolution
		self.ext_conv1 = nn.Sequential(
			nn.Conv2d(
				channels,
				internal_channels,
				kernel_size=1,
				stride=1,
				bias=bias), nn.BatchNorm2d(internal_channels), activation())

		# If the convolution is asymmetric we split the main convolution in
		# two. Eg. for a 5x5 asymmetric convolution we have two convolution:
		# the first is 5x1 and the second is 1x5.
		if asymmetric:
			self.ext_conv2 = nn.Sequential(
				nn.Conv2d(
					internal_channels,
					internal_channels,
					kernel_size=(kernel_size, 1),
					stride=1,
					padding=(padding, 0),
					dilation=dilation,
					bias=bias), nn.BatchNorm2d(internal_channels), activation(),
				nn.Conv2d(
					internal_channels,
					internal_channels,
					kernel_size=(1, kernel_size),
					stride=1,
					padding=(0, padding),
					dilation=dilation,
					bias=bias), nn.BatchNorm2d(internal_channels), activation())
		else:
			self.ext_conv2 = nn.Sequential(
				nn.Conv2d(
					internal_channels,
					internal_channels,
					kernel_size=kernel_size,
					stride=1,
					padding=padding,
					dilation=dilation,
					bias=bias), nn.BatchNorm2d(internal_channels), activation())

		# 1x1 expansion convolution
		self.ext_conv3 = nn.Sequential(
			nn.Conv2d(
				internal_channels,
				channels,
				kernel_size=1,
				stride=1,
				bias=bias), nn.BatchNorm2d(channels), activation())

		self.ext_regul = nn.Dropout2d(p=dropout_prob)

		# PReLU layer to apply after adding the branches
		self.out_activation = activation()

	def forward(self, x):
		# Main branch shortcut
		main = x

		# Extension branch
		ext = self.ext_conv1(x)
		ext = self.ext_conv2(ext)
		ext = self.ext_conv3(ext)
		ext = self.ext_regul(ext)

		# Add main and extension branches
		out = main + ext

		return self.out_activation(out)



class DownsamplingBottleneck(nn.Module):
    """Downsampling bottlenecks further downsample the feature map size.

    Main branch:
    1. max pooling with stride 2; indices are saved to be used for
    unpooling later.

    Extension branch:
    1. 2x2 convolution with stride 2 that decreases the number of channels
    by ``internal_ratio``, also called a projection;
    2. regular convolution (by default, 3x3);
    3. 1x1 convolution which increases the number of channels to
    ``out_channels``, also called an expansion;
    4. dropout as a regularizer.

    Keyword arguments:
    - in_channels (int): the number of input channels.
    - out_channels (int): the number of output channels.
    - internal_ratio (int, optional): a scale factor applied to ``channels``
    used to compute the number of channels after the projection. eg. given
    ``channels`` equal to 128 and internal_ratio equal to 2 the number of
    channels after the projection is 64. Default: 4.
    - return_indices (bool, optional):  if ``True``, will return the max
    indices along with the outputs. Useful when unpooling later.
    - dropout_prob (float, optional): probability of an element to be
    zeroed. Default: 0 (no dropout).
    - bias (bool, optional): Adds a learnable bias to the output if
    ``True``. Default: False.
    - relu (bool, optional): When ``True`` ReLU is used as the activation
    function; otherwise, PReLU is used. Default: True.

    """

    def __init__(self,
                 in_channels,
                 out_channels,
                 internal_ratio=4,
                 return_indices=True,
                 dropout_prob=0,
                 bias=False,
                 relu=True):
        super().__init__()

        # Store parameters that are needed later
        self.return_indices = return_indices

        # Check in the internal_scale parameter is within the expected range
        # [1, channels]
        if internal_ratio <= 1 or internal_ratio > in_channels:
            raise RuntimeError("Value out of range. Expected value in the "
                               "interval [1, {0}], got internal_scale={1}. "
                               .format(in_channels, internal_ratio))

        internal_channels = in_channels // internal_ratio

        if relu:
            activation = nn.ReLU
        else:
            activation = nn.PReLU

        # Main branch - max pooling followed by feature map (channels) padding
        self.main_max1 = nn.MaxPool2d(
            2,
            stride=2,
            return_indices=return_indices)

        # Extension branch - 2x2 convolution, followed by a regular, dilated or
        # asymmetric convolution, followed by another 1x1 convolution. Number
        # of channels is doubled.

        # 2x2 projection convolution with stride 2
        self.ext_conv1 = nn.Sequential(
            nn.Conv2d(
                in_channels,
                internal_channels,
                kernel_size=2,
                stride=2,
                bias=bias), nn.BatchNorm2d(internal_channels), activation())

        # Convolution
        self.ext_conv2 = nn.Sequential(
            nn.Conv2d(
                internal_channels,
                internal_channels,
                kernel_size=3,
                stride=1,
                padding=1,
                bias=bias), nn.BatchNorm2d(internal_channels), activation())

        # 1x1 expansion convolution
        self.ext_conv3 = nn.Sequential(
            nn.Conv2d(
                internal_channels,
                out_channels,
                kernel_size=1,
                stride=1,
                bias=bias), nn.BatchNorm2d(out_channels), activation())

        self.ext_regul = nn.Dropout2d(p=dropout_prob)

        # PReLU layer to apply after concatenating the branches
        self.out_activation = activation()

    def forward(self, x):
        # Main branch shortcut
        if self.return_indices:
            main, max_indices = self.main_max1(x)
        else:
            main = self.main_max1(x)

        # Extension branch
        ext = self.ext_conv1(x)
        ext = self.ext_conv2(ext)
        ext = self.ext_conv3(ext)
        ext = self.ext_regul(ext)

        # Main branch channel padding
        n, ch_ext, h, w = ext.size()
        ch_main = main.size()[1]
        padding = torch.zeros(n, ch_ext - ch_main, h, w)

        # Before concatenating, check if main is on the CPU or GPU and
        # convert padding accordingly
        if main.is_cuda:
            padding = padding.cuda()

        # Concatenate
        main = torch.cat((main, padding), 1)

        # Add main and extension branches
        out = main + ext

        return self.out_activation(out), max_indices



class Detect(nn.Module):
	def __init__(self, nc=80, anchors=(), ch=()):  # detection layer
		super(Detect, self).__init__()
		self.stride = None  # strides computed during build
		self.nc = nc  # number of classes
		self.no = nc + 5  # number of outputs per anchor
		self.nl = len(anchors)  # number of detection layers
		self.na = len(anchors[0]) // 2  # number of anchors
		self.grid = [torch.zeros(1)] * self.nl  # init grid
		a = torch.tensor(anchors).float().view(self.nl, -1, 2)
		self.register_buffer('anchors', a)  # shape(nl,na,2)
		self.register_buffer('anchor_grid', a.clone().view(self.nl, 1, -1, 1, 1, 2))  # shape(nl,1,na,1,1,2)
		self.m = nn.ModuleList(nn.Conv2d(x, self.no * self.na, 1) for x in ch)  # output conv
		self.export = False  # onnx export

	def forward(self, x):
		# x = x.copy()  # for profiling
		z = []  # inference output
		self.training |= self.export
		for i in range(self.nl):
			x[i] = self.m[i](x[i])  # conv
			bs, _, ny, nx = x[i].shape  # x(bs,255,20,20) to x(bs,3,20,20,85)
			x[i] = x[i].view(bs, self.na, self.no, ny, nx).permute(0, 1, 3, 4, 2).contiguous()

			if not self.training:  # inference
				if self.grid[i].shape[2:4] != x[i].shape[2:4]:
					self.grid[i] = self._make_grid(nx, ny).to(x[i].device)

				y = x[i].sigmoid()
				print(y[0])
				y[..., 0:2] = (y[..., 0:2] * 2. - 0.5 + self.grid[i].to(x[i].device)) * self.stride[i]  # xy
				y[..., 2:4] = (y[..., 2:4] * 2) ** 2 * self.anchor_grid[i]  # wh
				z.append(y.view(bs, -1, self.no))

		return x if self.training else (torch.cat(z, 1), x)

	@staticmethod
	def _make_grid(nx=20, ny=20):
		yv, xv = torch.meshgrid([torch.arange(ny), torch.arange(nx)])
		return torch.stack((xv, yv), 2).view((1, 1, ny, nx, 2)).float()

class MultEYE(nn.Module):
	def __init__(self, nc_detection, nc_segmentation, encoder_relu=False, decoder_relu=True):
		super(MultEYE, self).__init__()

		self.num_classes_detection = nc_detection
		self.num_classes_segmentation = nc_segmentation
		self.anchors = [[10,13, 16,30, 33,23], [30,61, 62,45, 59,119], [116,90, 156,198, 373,326]]
		##### Encoder Stage #####
		self.focus = Focus(3, 64, k=3)
		self.conv2 = Conv(64, 128, k=3, s=2)
		self.bottleneck_1_0 = DownsamplingBottleneck(128, 128)
		self.bottleneck_1_1 = BottleneckCSP(128, 128)
		self.bottleneck_1_2 = BottleneckCSP(128, 128)
		self.bottleneck_1_3 = BottleneckCSP(128, 128)
		self.conv4 = Conv(128, 256, k=3, s=2)
		self.bottleneck_2_0 = DownsamplingBottleneck(256, 256)
		self.bottleneck_2_1 = BottleneckCSP(256, 256)
		self.bottleneck_2_2 = BottleneckCSP(256, 256)
		self.bottleneck_2_3 = BottleneckCSP(256, 256)
		self.bottleneck_2_4 = BottleneckCSP(256, 256)
		self.bottleneck_2_5 = BottleneckCSP(256, 256)
		self.bottleneck_2_6 = BottleneckCSP(256, 256)
		self.bottleneck_2_7 = BottleneckCSP(256, 256)
		self.bottleneck_2_8 = BottleneckCSP(256, 256)
		self.bottleneck_2_9 = BottleneckCSP(256, 256)
		self.conv6 = Conv(256, 512, k=3, s=2)
		self.bottleneck_3_1 = BottleneckCSP(512, 512)
		self.bottleneck_3_2 = BottleneckCSP(512, 512)
		self.bottleneck_3_3 = BottleneckCSP(512, 512)
		self.bottleneck_3_4 = BottleneckCSP(512, 512)
		self.bottleneck_3_5 = BottleneckCSP(512, 512)
		self.bottleneck_3_6 = BottleneckCSP(512, 512)
		self.bottleneck_3_7 = BottleneckCSP(512, 512)
		self.bottleneck_3_8 = BottleneckCSP(512, 512)
		self.bottleneck_3_9 = BottleneckCSP(512, 512)
		self.conv8 = Conv(512, 1024, k=3, s=2)
		self.spp = SPP(1024, 1024)
		self.bottleneck_4_1 = BottleneckCSP(1024, 1024, shortcut=False)
		self.bottleneck_4_2 = BottleneckCSP(1024, 1024, shortcut=False)
		self.bottleneck_4_3 = BottleneckCSP(1024, 1024, shortcut=False)

		##### Detection Stage #####
		self.conv11 = Conv(1024, 512, k=1, s=1)
		self.bottleneck_5_1 = BottleneckCSP(1024, 1024, shortcut=False)
		self.bottleneck_5_2 = BottleneckCSP(1024, 1024, shortcut=False)
		self.bottleneck_5_3 = BottleneckCSP(1024, 1024, shortcut=False)
		self.conv14 = Conv(1024, 256, k=1, s=1)
		self.bottleneck_6_1 = BottleneckCSP(512, 512, shortcut=False)
		self.bottleneck_6_2 = BottleneckCSP(512, 512, shortcut=False)
		self.bottleneck_6_3 = BottleneckCSP(512, 512, shortcut=False)
		self.conv18 = Conv(512, 256, k=3, s=2)
		self.bottleneck_7_1 = BottleneckCSP(512, 512, shortcut=False)
		self.bottleneck_7_2 = BottleneckCSP(512, 512, shortcut=False)
		self.bottleneck_7_3 = BottleneckCSP(512, 512, shortcut=False)
		self.conv20 = Conv(512, 512, k=3, s=2)
		self.bottleneck_8_1 = BottleneckCSP(1024, 1024, shortcut=False)
		self.bottleneck_8_2 = BottleneckCSP(1024, 1024, shortcut=False)
		self.bottleneck_8_3 = BottleneckCSP(1024, 1024, shortcut=False)
		self.output = Detect(self.num_classes_detection, anchors=self.anchors, ch=[512, 512, 1024])
		m=self.output
		##### Segmentation Head #####

		self.upsample4_0 = UpsamplingBottleneck(
			1024, 256, dropout_prob=0.1, relu=decoder_relu)
		self.regular4_1 = RegularBottleneck(
			256, padding=1, dropout_prob=0.1, relu=decoder_relu)
		self.regular4_2 = RegularBottleneck(
			256, padding=1, dropout_prob=0.1, relu=decoder_relu)

		# Stage 5 - Decoder
		self.upsample5_0 = UpsamplingBottleneck(
			256, 128, dropout_prob=0.1, relu=decoder_relu)
		self.regular5_1 = RegularBottleneck(
			128, padding=1, dropout_prob=0.1, relu=decoder_relu)
		self.regular5_2 = RegularBottleneck(
			256, padding=1, dropout_prob=0.1, relu=decoder_relu)
		self.transposed_conv = nn.ConvTranspose2d(
			128,
			nc_segmentation,
			kernel_size=1,
			stride=2,
			padding=1,
			bias=False)

		s = 512  # 2x min stride
		m.stride = torch.tensor([s / x.shape[-2] for x in self.forward(torch.zeros(5, 3, s, s))])  # forward
		m.anchors /= m.stride.view(-1, 1, 1)
		check_anchor_order(m)
		self.stride = m.stride
		self._initialize_biases()

	def forward(self, x):
		input_size = x.size()
		##### Backbone #####

		x = self.focus(x)
		
		x = self.conv2(x)
		stage1_input_size = x.size()
		x, max_indices1_0 = self.bottleneck_1_0(x)
		
		x_conv4_p3 = self.conv4(x)
		x, max_indices2_0 = self.bottleneck_2_0(x_conv4_p3)
		x = self.bottleneck_2_1(x_conv4_p3)
		x = self.bottleneck_2_2(x)
		stage2_input_size = x.size()
		x_conv6_p4 = self.conv6(x)
		x = self.bottleneck_3_1(x_conv6_p4)
		x = self.bottleneck_3_2(x)
		x = self.bottleneck_3_3(x)
		x_conv8_p5 = self.conv8(x)
		x = self.spp(x_conv8_p5)
		x_backbone = self.bottleneck_4_3(x)

		##### Detection Stage #####
		
		x_conv10_p6 = self.conv11(x_backbone)
		x_conv10_p6_up = F.interpolate(x_conv10_p6, scale_factor=2, mode='nearest')
		x = torch.cat([x_conv10_p6_up, x_conv6_p4], dim=1)
		x = self.bottleneck_5_1(x)
		x_conv14_p7 = self.conv14(x)
		x = F.interpolate(x_conv14_p7, scale_factor=2, mode='nearest')
		x_1 = torch.cat([x, x_conv4_p3], dim=1)
		x = self.bottleneck_6_1(x_1)
		x_conv18_p8 = self.conv18(x)
		x_2 = torch.cat([x_conv18_p8, x_conv14_p7], dim=1)
		x = self.bottleneck_7_1(x_2)
		x_conv20_p8 = self.conv20(x)
		#print(x_conv20_p8.shape, x_conv18_p8.shape, x_conv14_p7.shape, x_conv10_p6.shape, x_conv10_p6_up.shape)
		x_3 = torch.cat([x_conv20_p8, x_conv10_p6], dim=1)

		backbone_final = self.bottleneck_8_3(x_3)
		detection_output = self.output([x_1, x_2, x_3])

		##### Segmentation Stage #####

		# Stage 4 - Decoder
		x_backbone = F.interpolate(x_backbone, scale_factor=2, mode='nearest')
		x_seg = self.upsample4_0(x_backbone, max_indices2_0, output_size=stage2_input_size)
		x_seg = self.regular4_1(x_seg)
		x_seg = self.regular4_2(x_seg)

		# Stage 5 - Decoder
		x_seg = F.interpolate(x_seg, scale_factor=2, mode='nearest')
		x_seg = self.upsample5_0(x_seg, max_indices1_0, output_size=stage1_input_size)
		x_seg = self.regular5_1(x_seg)
		x_seg = F.interpolate(x_seg, scale_factor=2, mode='nearest')
		#x_seg = self.transposed_conv(x_seg, output_size=input_size)
		return detection_output,x_seg

	def _initialize_biases(self, cf=None):  # initialize biases into Detect(), cf is class frequency
	 # cf = torch.bincount(torch.tensor(np.concatenate(dataset.labels, 0)[:, 0]).long(), minlength=nc) + 1.
		m = self.model[-1]  # Detect() module
		for mi, s in zip(m.m, m.stride):  # from
			b = mi.bias.view(m.na, -1)  # conv.bias(255) to (3,85)
			b[:, 4] += math.log(8 / (640 / s) ** 2)  # obj (8 objects per 640 image)
			b[:, 5:] += math.log(0.6 / (m.nc - 0.99)) if cf is None else torch.log(cf / cf.sum())  # cls
			mi.bias = torch.nn.Parameter(b.view(-1), requires_grad=True)

	def _print_biases(self):
		m = self.model[-1]  # Detect() module
		for mi in m.m:  # from
			b = mi.bias.detach().view(m.na, -1).T 

if __name__=='__main__':
	model = MultEYE(1, 12)
	model.cuda()
	model.eval()
	img = torch.randn(5, 3, 512, 512).cuda()
	out_detect, out_seg = model(img)
	print(out_detect.shape)
