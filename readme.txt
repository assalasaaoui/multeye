For only detection, run

python detect.py --weights MultEYE.pt --img 512 --conf 0.2 --source soda.jpg

The source argument can be a video, image, image folder or a (camera)stream
Result is saved in inference/output

For Multi-Object Tracking with non-parametric speed estimation

python mot.py --weights MultEYE.pt --img 512 --conf 0.2 --source test.mp4

The mot.py tested with video but should work for streams too. Doesn't work for image folder.